---
title: Beating the Odds - Finding a reliable Betting Strategy for next Soccer Season
date: 2019-07-06
tags: ["football", "betting", "arbitrage", github", "jupyternotebook", "python"]
---

During this years summer vacation I had the idea to get back to some work I did a few years back. I am speaking of my analysis of the European Football Betting Market I did for my BA thesis. The basic idea was to test the *"Efficient Market Hypothesis"* using as much football data from the most important leagues in Europe I could get my hands on back in the days.

I always liked the thrill from betting on soccer games. But after some very boring seasons in Germany ([thank you very much FC Bayern Munich](https://en.wikipedia.org/wiki/FC_Bayern_Munich#Domestic)), I pretty much lost the interest in watching and betting.

So I sat down and asked myself, if I could find any reliable strategy to use for next season. I started to gather some data and write down python code in a jupyter notebook. I decided to devide the analysis into two parts:

1. Part one will cover getting the data, looking at the data set, data wrangling and arbitrage analysis
2. In part two I will check on the most widespread strategies in football betting (i.e. the famous *favourite longshot-bias*)

You can find the [part one jupyter notebook on my GitHub](https://github.com/mjt91/soccerbetting/blob/master/Testing%20Betting%20Strategies%20in%20German%20Football%201.%20Bundesliga%20(Part%201).ipynb).

I am thankful for any remarks and comments, just feel free to message me via [email](mailto:mariustheiss@gmail.com).