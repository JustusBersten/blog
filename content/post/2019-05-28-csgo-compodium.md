---
title: First post! Checkout my CS:GO Compendium
date: 2019-05-28
tags: ["cs:go", "github"]
---

![Valve's Counter-Strike: Global Offensive](https://steamcdn-a.akamaihd.net/steam/subs/54029/header_586x192.jpg?t=1544227353)

I want to start this blog with one of my dearest hobbies of all time. The arguably mother of all competitive games: ***Counter-Strike***. The most recent incarnation by the tech giant *Valve* is *Counter-Strike: Global Offensive*, which is very good and you should [give it a try](https://store.steampowered.com/app/730/CounterStrike_Global_Offensive/) if you are into first-person shooters.

I have been a big fan for over 13 years now. In fact, my [Steam Account](https://steamcommunity.com/id/phecks/) was created October 18th 2005! And Counter-Strike was my first game I added to my library. 

Looking back, gaming over the internet was quite a big deal back then. Connections were quite unstable and download rates were slow. There were many, many theories (see [[1]](https://wwwx.cs.unc.edu/~sparkst/howto/network_tuning.php), [[2]](http://counterstrike.4pforen.4players.de/viewtopic.php?t=126888&sid=db0cdec31e5ac56787911fcc622f340e), [[3]](http://counterstrike.4pforen.4players.de/viewtopic.php?t=126888&sid=db0cdec31e5ac56787911fcc622f340e)) on how to improve and have the edge over your opponent.

For us young kids it was normal to format your computers on a regular basis. We set up a complete new Windows before going on LAN or just to have a fresh Windows installation that runs Counter-Strike smoothly as ever. And to add: We did not have the the luxury of using fast SSDs but some slow  [Western Digital HDDs](https://www.tomshardware.com/reviews/hdd-terabyte-1tb,2077-6.html). But to be honest, we definitely loved it!

And I was one of those script kids too. And over the time I found quite a confident setup and my friends are often asking me for tips and my configuration files.

For this reason I created a short site I host on my [GitHub](https://github.com/mjt91) where I store all my settings, files and little tips and tricks. Follow this link and visit my [CS:GO Compendium](https://mjt91.github.io/abgehen/).

