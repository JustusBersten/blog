## Welcome to my Blog

On this site I try to collect some of my projects, thoughts about topics I find quite interesting or any other stuff I think is relevant to post here. I hope you will find joy in reading some of the stuff that appears here.

ps: This website is powered by [GitLab Pages](https://about.gitlab.com/features/pages/) / [Hugo](https://gohugo.io). It uses the `beautifulhugo` theme which supports content on your front page.
I followed [this](https://brainfood.xyz/post/20190518-host-your-own-blog-in-1-hour/) tutorial by Christian Mäder to set up this blog and host it on GitLab.

