---
title: About me
subtitle: How I became the awesome bro I am today
comments: false
---
### History

My name is Marius and I am an early 90s kid. I have a M.Sc. in Business Mathematics. I am mostly interested in Finance, Game Theory, competitive computer games and driving downhill with my mountain bike. I mainly use python and sometimes R. I drink beer.


### Tools I use

- Mostly Windows 10
- SublimeText 3
- Hack Font (get it [HERE](https://github.com/source-foundry/Hack))
- pyCharm for python development (using Hack font and THEME XYZ)


### People that inspire me

- NYT Bestseller author **Tucker Max**. Check out
    + his books *"I Hope They Serve Beer in Hell"* and *"Assholes Finish First"*
    + his [blog](http://tuckermax.me/)
- Gordon Ramsay
- Roger Federer

